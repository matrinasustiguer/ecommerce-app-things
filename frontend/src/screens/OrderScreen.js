import React, {useState, useEffect} from 'react'
import axios from 'axios'
import { PayPalButton } from 'react-paypal-button-v2'
import { Link } from 'react-router-dom';
import { Row, Col, ListGroup, Image, Card, Button } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import Loader from '../components/Loader'
import Meta from '../components/Meta';
import { FREE_SHIPPING_MINIMUM, ORDER_DELIVER_RESET, ORDER_PAY_RESET, VAT_EXEMPT, ZERO_RATED } from '../constants/orderConstants';
import { deliverOrder, getOrderDetails, payOrder } from '../actions/orderActions'

const OrderScreen = ({match, history}) => {
    const orderId = match.params.id

    const [sdkReady, setSdkReady] = useState(false)

    const dispatch = useDispatch()
    
    const orderDetails = useSelector(state => state.orderDetails)
    const { order, loading, error } = orderDetails

    const orderPay = useSelector(state => state.orderPay)
    const { loading:loadingPay, success:successPay } = orderPay

    const orderDeliver = useSelector(state => state.orderDeliver)
    const { loading:loadingDeliver, success:successDeliver } = orderDeliver

    const userLogin = useSelector(state => state.userLogin)
    const { userInfo } = userLogin
    
    if(!loading) {
        // Calculate prices
        const addDecimals = (num) => {
            return (Math.round(num * 100) / 100).toFixed(2)
        }
        order.itemsPrice = addDecimals(order.orderItems.reduce((acc, item) => acc + item.price * item.qty, 0))
        order.zeroRated = addDecimals(ZERO_RATED)
        order.vatExempt = addDecimals(VAT_EXEMPT)
    }

    useEffect(() => {
        if(!userInfo){
            history.push('/login')
        }

        const addPayPalScript = async () => {
            const { data: clientId } = await axios.get('/api/config/paypal')
            const script = document.createElement('script')
            script.type = 'text/javascript'
            script.src = `https://www.paypal.com/sdk/js?client-id=${clientId}&currency=PHP`
            script.async = true
            script.onload = () => {
                setSdkReady(true)
            }
            document.body.appendChild(script)
        }

        if(!order || successPay || order._id !== orderId || successDeliver){
            dispatch({ type: ORDER_PAY_RESET })
            dispatch({ type: ORDER_DELIVER_RESET })
            dispatch(getOrderDetails(orderId))
        } else if(!order.isPaid) {
            if(!window.paypal) {
                addPayPalScript()
            } else {
                setSdkReady(true)
            }
        }
    },[dispatch, order, orderId, successPay, successDeliver, history, userInfo])

    const successPaymentHandler = (paymentResult) => {
        dispatch(payOrder(orderId, paymentResult))
    }

    const deliverHandler = () => {
        dispatch(deliverOrder(order))
    }

    return (
        loading ? <Loader />
        : error ? <Message variant='danger'>{error}</Message>
        : <>
            <Meta title='Things © Online Shop | Order Details'/>
            <h1>Order {order._id}</h1>
            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item>
                            <h2>Shipping</h2>
                            <p className='mb-0'><strong>Name: </strong>{order.user.name}</p>
                            <p className='mb-0'>
                                <strong>Email: </strong>
                                <a href={`mailto:${order.user.email}`}>{order.user.email}</a>
                            </p>
                            <p>
                                <strong>Address:</strong>
                                {order.shippingAddress.address}, {order.shippingAddress.city}{' '}
                                {order.shippingAddress.postalCode},{' '}
                                {order.shippingAddress.country}
                            </p>
                            {order.isDelivered ? <Message variant='success'>Delivered on {order.deliveredAt}</Message>
                            : <Message variant='danger'>Not Yet Delivered</Message>
                            }
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Payment Method</h2>
                            <p>
                                <strong>Method: </strong>
                                {order.paymentMethod}
                            </p>
                            {order.isPaid ? <Message variant='success'>Paid on {order.paidAt}</Message>
                            : <Message variant='danger'>Not Yet Paid</Message>
                            }
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Order Items</h2>
                            {order.orderItems.length === 0 ? 
                            <Message>Order is empty.</Message>
                            : (
                                <ListGroup variant='fllush'>
                                    {order.orderItems.map((item, index) => (
                                        <ListGroup.Item key={index}>
                                            <Row>
                                                <Col md={1}>
                                                    <Image src={item.image} alt={item.name} fluid rounded /> 
                                                </Col>
                                                <Col>
                                                    <Link to={`/product/${item.product}`}>{item.name}</Link>
                                                </Col>
                                                <Col md={4}>
                                                    {item.qty} x &#8369;{item.price} = &#8369;{item.qty * item.price}
                                                </Col>
                                            </Row>
                                        </ListGroup.Item>
                                    ))}
                                </ListGroup>
                            )}
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md={4}>
                    <Card>
                        <ListGroup variant='flush'>
                            <ListGroup.Item variant='primary'>
                                <h2>Order Summary</h2>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Row>
                                    <Col>
                                        <p className='m-0'><small>Vatable</small></p>
                                        <p className='m-0'><small>VAT Exempt</small></p>
                                        <p className='m-0'><small>Zero Rated</small></p>
                                        <p className='m-0'><small>Value Added Tax</small></p>
                                        <p className='m-0'><small>Total with Tax</small></p>
                                    </Col>
                                    <Col>
                                        <p className='m-0'><small>&#8369;{order.vatablePrice}</small></p>
                                        <p className='m-0'><small>&#8369;{order.taxPrice}</small></p>
                                        <p className='m-0'><small>&#8369;{order.vatExempt}</small></p>
                                        <p className='m-0'><small>&#8369;{order.zeroRated}</small></p>
                                        <p className='m-0'><small>&#8369;{order.itemsPrice}</small></p>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>Items</Col>
                                    <Col>&#8369;{order.itemsPrice}</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>
                                        <p>Shipping</p>
                                        <small>
                                            <em>*Minimum of &#8369;{FREE_SHIPPING_MINIMUM} worth of items to get free shipping</em>
                                        </small>
                                    </Col>
                                    <Col>&#8369;{order.shippingPrice}</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>Grand Total with Shipping</Col>
                                    <Col>
                                        <strong>&#8369;{order.totalPrice}</strong>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                            {!order.isPaid && (
                                <ListGroup.Item>
                                    {loadingPay && <Loader/>}
                                    {!sdkReady ? <Loader/> : (
                                        <PayPalButton 
                                            amount={order.totalPrice}
                                            currency='PHP'
                                            onSuccess={successPaymentHandler}
                                        />
                                    )}
                                </ListGroup.Item>
                            )}
                            {loadingDeliver && <Loader/>}
                            {userInfo && userInfo.isAdmin && order.isPaid && !order.isDelivered && (
                                <ListGroup.Item>
                                    <Button type='button' className='btn btn-block' onClick={deliverHandler}>
                                        Mark as Delivered
                                    </Button>
                                </ListGroup.Item>
                            )}
                        </ListGroup>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default OrderScreen
