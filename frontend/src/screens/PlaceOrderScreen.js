import React, {useEffect} from 'react'
import { Link } from 'react-router-dom';
import { Button, Row, Col, ListGroup, Image, Card } from 'react-bootstrap'
import { useDispatch, useSelector } from 'react-redux'
import Message from '../components/Message'
import CheckoutSteps from '../components/CheckoutSteps'
import Meta from '../components/Meta';
import { FREE_SHIPPING_MINIMUM, ORDER_CREATE_RESET, VAT_EXEMPT, ZERO_RATED } from '../constants/orderConstants';
import { createOrder } from '../actions/orderActions'
import { resetCart } from '../actions/cartActions';

const PlaceOrderScreen = ({history}) => {
    const dispatch = useDispatch()

    const cart = useSelector(state => state.cart)
    
    // Calculate prices
    const addDecimals = (num) => {
        return (Math.round(num * 100) / 100).toFixed(2)
    }
    cart.itemsPrice = addDecimals(cart.cartItems.reduce((acc, item) => acc + item.price * item.qty, 0))

    cart.vatExempt = addDecimals(VAT_EXEMPT)
    cart.zeroRated = addDecimals(ZERO_RATED)
    cart.vatablePrice = addDecimals(Number((cart.itemsPrice / 1.12 * 1).toFixed(2)))
    cart.taxPrice = addDecimals(Number((cart.itemsPrice / 1.12 * 0.12).toFixed(2)))
    
    cart.shippingPrice = addDecimals(cart.itemsPrice > FREE_SHIPPING_MINIMUM ? 0 : 50)

    cart.totalPrice = (Number(cart.itemsPrice) + Number(cart.shippingPrice)).toFixed(2)

    const orderCreate = useSelector(state => state.orderCreate)
    const { order, success, error } = orderCreate

    useEffect(() => {
        if(success) {
            dispatch({type: ORDER_CREATE_RESET})
            dispatch(resetCart())
            history.push(`/order/${order._id}`)
        }
        // eslint-disable-next-line
    },[history, success])

    const placeOrderHandler = () => {
        dispatch(
            createOrder({
                orderItems: cart.cartItems,
                shippingAddress: cart.shippingAddress,
                paymentMethod: cart.paymentMethod,
                itemsPrice: cart.itemsPrice,
                shippingPrice: cart.shippingPrice,
                vatablePrice: cart.vatablePrice,
                taxPrice: cart.taxPrice,
                totalPrice: cart.totalPrice,
        }))
    }

    return (
        <>
        <Meta title='Things© | Order Placement'/>
            <CheckoutSteps step1 step2 step3 step4/>
            <Row>
                <Col md={8}>
                    <ListGroup variant='flush'>
                        <ListGroup.Item>
                            <h2>Shipping</h2>
                            <p>
                                <strong>Address:</strong>
                                {cart.shippingAddress.address}, {cart.shippingAddress.city}{' '}
                                {cart.shippingAddress.postalCode},{' '}
                                {cart.shippingAddress.country}
                            </p>
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Payment Method</h2>
                            <strong>Method: </strong>
                            {cart.paymentMethod}
                        </ListGroup.Item>

                        <ListGroup.Item>
                            <h2>Order Items</h2>
                            {cart.cartItems.length === 0 ? 
                            <Message>Your cart is empty.</Message>
                            : (
                                <ListGroup variant='fllush'>
                                    {cart.cartItems.map((item, index) => (
                                        <ListGroup.Item key={index}>
                                            <Row>
                                                <Col md={1}>
                                                    <Image src={item.image} alt={item.name} fluid rounded /> 
                                                </Col>
                                                <Col>
                                                    <Link to={`/product/${item.product}`}>{item.name}</Link>
                                                </Col>
                                                <Col md={4}>
                                                    {item.qty} x &#8369;{item.price} = &#8369;{item.qty * item.price}
                                                </Col>
                                            </Row>
                                        </ListGroup.Item>
                                    ))}
                                </ListGroup>
                            )}
                        </ListGroup.Item>
                    </ListGroup>
                </Col>
                <Col md={4}>
                    <Card>
                        <ListGroup variant='flush'>
                            <ListGroup.Item variant='primary'>
                                <h2>Order Summary</h2>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Row>
                                    <Col>
                                        <p className='m-0'><small>Vatable</small></p>
                                        <p className='m-0'><small>VAT Exempt</small></p>
                                        <p className='m-0'><small>Zero Rated</small></p>
                                        <p className='m-0'><small>Value Added Tax</small></p>
                                        <p className='m-0'><small>Total with Tax</small></p>
                                    </Col>
                                    <Col>
                                        <p className='m-0'><small>&#8369;{cart.vatablePrice}</small></p>
                                        <p className='m-0'><small>&#8369;{cart.taxPrice}</small></p>
                                        <p className='m-0'><small>&#8369;{cart.vatExempt}</small></p>
                                        <p className='m-0'><small>&#8369;{cart.zeroRated}</small></p>
                                        <p className='m-0'><small>&#8369;{cart.itemsPrice}</small></p>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>Items</Col>
                                    <Col>&#8369;{cart.itemsPrice}</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>
                                        <p>Shipping</p>
                                        <small>
                                            <em>*Minimum of &#8369;{FREE_SHIPPING_MINIMUM} worth of items to get free shipping</em>
                                        </small>
                                    </Col>
                                    <Col>&#8369;{cart.shippingPrice}</Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item variant='primary'>
                                <Row>
                                    <Col>Grand Total with Shipping</Col>
                                    <Col>
                                        <strong>&#8369;{cart.totalPrice}</strong>
                                    </Col>
                                </Row>
                            </ListGroup.Item>
                            <ListGroup.Item>
                                {error && <Message variant='danger'>{error}</Message>}
                            </ListGroup.Item>
                            <ListGroup.Item>
                                <Button type='button' className='btn-block'
                                disable = {cart.cartItems === 0 ? true : false} onClick={placeOrderHandler}
                                >
                                    Place Order
                                </Button>
                            </ListGroup.Item>
                        </ListGroup>
                    </Card>
                </Col>
            </Row>
        </>
    )
}

export default PlaceOrderScreen
