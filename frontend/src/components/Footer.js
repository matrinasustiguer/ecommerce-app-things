import React from 'react'
import { Container, Row, Col } from 'react-bootstrap'
const Footer = () => {
    return (
        <footer>
            <Container>
                <Row>
                    <Col className='text-center py-3'>
                        This is a fictional, non-endorsed, non-sponsored site meant for educational purposes only. All assets, logos, and trademarks used and/or referenced on this site are the assets, trademarks, and logos of their respective owners. Copyright &copy; Things.
                    </Col>
                </Row>
            </Container>
        </footer>
    )
}

export default Footer
