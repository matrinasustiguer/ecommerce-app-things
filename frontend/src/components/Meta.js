import React from 'react'
import { Helmet } from 'react-helmet'

const Meta = ({ title, description, keywords }) => {
    return (
        <Helmet>
            <title>{title}</title>
            <meta name='description' content={description}></meta>
            <meta name='keywords' content={keywords}></meta>
        </Helmet>
    )
}

Meta.defaultProps = {
    title: 'Things © Online Shop',
    description: 'Come and buy great products at a very low price',
    keywords: 'health, fitness, art, craft, puzzle, board game, boardgame, buy, online shop',
}

export default Meta
