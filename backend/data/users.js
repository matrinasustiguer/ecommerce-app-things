import bcrypt from 'bcryptjs'

const users = [
    {
        name: 'Admin User',
        email: 'admin@user.com',
        password: bcrypt.hashSync('123admin', 10),
        isAdmin: true
    },
    {
        name: 'John Cruz',
        email: 'john@user.com',
        password: bcrypt.hashSync('123john', 10)
    },
    {
        name: 'Jane Cruz',
        email: 'jane@user.com',
        password: bcrypt.hashSync('123jane', 10)
    },
    {
        name: 'Guest User',
        email: 'guest@user.com',
        password: bcrypt.hashSync('123guest', 10)
    },
]

export default users