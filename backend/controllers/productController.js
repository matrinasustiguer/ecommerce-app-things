import asyncHandler from 'express-async-handler'
import Product from '../models/productModel.js'

// Fetches all products via route: GET /api/products with public access
const getProducts = asyncHandler(async (req, res) => {
    const pageSize = 12
    const page = Number(req.query.pageNumber) || 1

    const keyword = req.query.keyword ? {
        name : {
            $regex: req.query.keyword,
            $options: 'i'
        }
    } : {}

    const count = await Product.countDocuments({...keyword})
    const products = await Product.find({...keyword})
        .limit(pageSize)
        .skip(pageSize * (page - 1))
    
    res.json({ products, page, pages: Math.ceil(count / pageSize) })
})

// Fetches a single product via route: GET /api/products/:id with public access
const getProductById = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
        res.json(product)
    } else {
        res.status(404)
        throw new Error('Product Not Found')
    }
})

// Delete a product via route: DELETE /api/products/:id with private/admin access
const deleteProduct = asyncHandler(async (req, res) => {
    const product = await Product.findById(req.params.id)

    if (product) {
        await product.remove()
        res.json({message: 'Product Removed'})
    } else {
        res.status(404)
        throw new Error('Product Not Found')
    }
})

// Create a product via route: POST /api/products/ with private/admin access
const createProduct = asyncHandler(async (req, res) => {
    const product = new Product({
        name: 'Sample Name',
        price: 0,
        description: 'Sample Description',
        user: req.user._id,
        image: '/images/sample.jpg',
        brand: 'Sample Brand',
        category: 'Sample Category',
        countInStock: 0,
        numReviews: 0
    })

    const createdProduct = await product.save()
    res.status(201).json(createdProduct)
})

// Update a product via route: PUT /api/products/ with private/admin access
const updateProduct = asyncHandler(async (req, res) => {
    const { name, price, description, image, brand, category, countInStock } = req.body

    const product = await Product.findById(req.params.id)

    if(product) {
        product.name = name
        product.price = price
        product.description = description
        product.image = image
        product.brand = brand
        product.category = category
        product.countInStock = countInStock

        const updatedProduct = await product.save()
        res.json(updatedProduct)
    } else {
        res.status(404)
        throw new Error('Product Not Found')
    }
})

// Create a new review via route: POST /api/products/:id/reviews with private access
const createProductReview = asyncHandler(async (req, res) => {
    const { rating, comment } = req.body

    const product = await Product.findById(req.params.id)

    if(product) {
        const alreadyReviewed = product.reviews.find(r => r.user.toString() === req.user._id.toString())

        if(alreadyReviewed){
            res.status(400)
            throw new Error('You Already Reviewed This Product')
        }

        const review = {
            name: req.user.name,
            rating: Number(rating),
            comment,
            user: req.user._id
        }

        product.reviews.push(review)

        product.numReviews = product.reviews.length

        product.rating = product.reviews.reduce((acc, item) => item.rating + acc, 0) / product.reviews.length

        await product.save()
        res.status(201).json({ message: 'Review Successfully Added' })
    } else {
        res.status(404)
        throw new Error('Product Not Found')
    }
})

// Get top rated products via route: GET /api/products/top with public access
const getTopProduct = asyncHandler(async (req, res) => {
    const products = await Product.find({}).sort({ rating: -1}).limit(3)

    res.json(products)
})

export { getProducts, getProductById, deleteProduct, createProduct, updateProduct, createProductReview, getTopProduct }